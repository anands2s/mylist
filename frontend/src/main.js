import Vue from 'vue'
import App from './App'
import router from './router'
import axios from 'axios'
import {API_URL} from './config.js';

require('../node_modules/bootstrap-sass/assets/stylesheets/_bootstrap.scss')

window.token=localStorage.getItem('token');
window.axios=axios
window.axios.defaults.baseURL='http://localhost/mylist/backend/'
Vue.config.productionTip = false
window.Event= new Vue;

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
})