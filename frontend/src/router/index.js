import Vue from 'vue'
import Router from 'vue-router'
import siteHeader from '@/components/site-header/index.vue'
import Register from '@/components/Register'
import Login from '@/components/Login'
import Users from '@/components/Users'
import Logout from '@/components/Logout'

Vue.use(Router)

export default new Router({
  routes: [
    { path: '/register', name: 'Register', component: Register },
    { path: '/login', name: 'Login', component: Login },
    { path: '/users', name: 'Users', component: Users },
    { path: '/logout', name: 'Logout', component: Logout }
  ],
  mode:'history'
})