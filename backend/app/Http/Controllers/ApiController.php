<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ApiController extends Controller
{

    /**
     * Users
     *
     * @return \Illuminate\Http\Response
     */
     public function users(Request $request)
    {
       $api_token = trim($request->api_token);

       if($api_token == "expired"){
            return response()->json(['status' => 'error', 'message' => 'Token expired'], 206);
       }

       $users = User::where('active', 1)
               ->orderBy('username', 'desc')
               ->take(20)
               ->get();

       if (!$users) {
            return response()->json(['status' => 'error', 'message' => 'No data'], 206);
       }

       return response()->json(['status' => 'success', 'users' => $users], 200);

    }

     /**
     * Users
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /**
         * Get a validator for an incoming registration request.
         *
         * @param  array  $request
         * @return \Illuminate\Contracts\Validation\Validator
        */

       $data = $request->all();

       return response()->json(['status' => 'success', 'user' => $data], 200);
       
        $validationRules = [
            'first_name' => 'required|string|',
            'last_name' => 'required|string|',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6'
        ];

        $validator = Validator::make($data, $validationRules);
        
        if ($validator->fails()) {
            $jsonError = $validator->errors()->all();
            return response()->json(['validation_errors' => $jsonError], 400);
        }
        
        $username = explode('@', $data['email']);

        $user = new User();
        $user->username = $username[0]."_".$username[1];
        $user->email = $data['email'];
        $user->password = app('hash')->make($data['password']);
        $user->api_token = str_random(50);
        $user->first_tname = $data['first_name'];
        $user->last_name = $data['last_name'];
        $save = $user->save();

        return response()->json(['status' => 'success', 'user' => $user], 200);

    }

    /**
     * Users
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id = $request->id;
        $user = User::find($id);

        if (!$user) {
            return response()->json(['status' => 'error', 'message' => 'unauthorized'], 401);
        }

        $user->update($request->all());

        return response()->json(['message' => 'success', 'user' => $user], 200);

    }

     /**
     * Users
     * 
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (User::destroy($id)) {
            return response()->json(['status' => 'success', 'message' => 'User deleted successfully']);
        }

        return response()->json(['status' => 'error', 'message' => 'Something went wrong']);

    }
}
