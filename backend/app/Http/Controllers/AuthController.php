<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{

    /**
     * Register
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {

        /**
         * Get a validator for an incoming registration request.
         *
         * @param  array  $request
         * @return \Illuminate\Contracts\Validation\Validator
        */

       $data = $request->all();
       
        $validationRules = [
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6'
        ];

        $validator = Validator::make($data, $validationRules);
        
        if ($validator->fails()) {
            $jsonError = $validator->errors()->all();
            return response()->json(['validation_errors' => $jsonError], 400);
        }
        
        $username = explode('@', $data['email']);

        $user = new User();
        $user->username = $username[0]."_".$username[1];
        $user->email = $data['email'];
        $user->password = app('hash')->make($data['password']);
        $user->api_token = str_random(50);
        $save = $user->save();

        return response()->json(['status' => 'success', 'user' => $user], 200);
    }

    /**
     * Login
     *
     * @return \Illuminate\Http\Response
    */
    public function login(Request $request)
    {
        $data = $request->only('email', 'password');

        $validationRules = [
            'email' => 'required|string|email|max:255',
            'password' => 'required|string|min:6'
        ];

        $validator = Validator::make($data, $validationRules);

        if ($validator->fails()) {
            $jsonError = $validator->errors()->all();
            return response()->json(['validation_errors' => $jsonError], 400);
        }

        $user = User::where('email', $request->email)->first();
        if (!$user) {
            return response()->json(['status' => 'error', 'message' => 'Sorry, the user details were not found. Please try again or register.'], 404);
        }

        if (Hash::check($request->password, $user->password)){

            $user->update(['api_token'=>str_random(50)]);
            return response()->json(['status' => 'success', 'user' => $user], 200);
        }

            return response()->json(['status' => 'error', 'message' => 'Invalid Credentials'], 401);
    }

     /**
     * Logout
     *
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $api_token = $request->api_token;

        $user = User::where('api_token', $api_token)->first();

        if (!$user) {
            return response()->json(['status' => 'error', 'message' => 'Not Logged in'], 401);
        }
        $user->api_token = null;

        $user->save();

            return response()->json(['status' => 'Success', 'message' => 'You are now logged out'], 200);

    }
}
