<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->post('/register','AuthController@register');
$router->post('/login','AuthController@login');

$router->group(['middleware' => 'auth'], function () use ($router) {    
	$router->get('/logout/{token}','AuthController@logout');
	$router->get('/users/{token}','ApiController@users');
	$router->post('/store','ApiController@store');
	$router->post('/update','ApiController@update');	
});

$router->delete('/destroy/{id}','ApiController@destroy');


// $router->post('/store', ['middleware' => 'auth',
// 	'uses' => 'ApiController@store'
// ]);

// $router->post('/profile', ['middleware' => 'auth',
// 	'uses' => 'ApiController@profile'
// ]);

// $router->get('users/{token}', function ($token) {
//     return $token;
// });

// $router->get('user/{id}', function ($id) {
//     return 'User '.$id;
// });

// $router->post('/login', function (\Illuminate\Http\Request $request)
// {
// 	dd("tttest");
//     dd(
//         $request->json()->get('email'),
//         $request->json()->all()
//     );
// });
// $router->post('login', function ($id) {
//     return $id;
// });

